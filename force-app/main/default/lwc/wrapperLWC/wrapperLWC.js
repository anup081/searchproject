import { LightningElement } from 'lwc';

export default class WrapperLWC extends LightningElement {
    greeting = 'World';

    handleChange(event) {
        this.greeting = event.target.value;
    }

    //getter variable
    get uppercasedFullName() {
        return `${this.greeting}`.toUpperCase();
    }
}