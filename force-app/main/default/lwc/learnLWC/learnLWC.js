import { LightningElement, api } from 'lwc';
import templateOne from './templateOne.html';
import templateTwo from './templateTwo.html';

export default class LearnLWC extends LightningElement {
    //firstName = 'World';
    @api firstName;
    areDetailsVisible = false;
    templateOne = true;

    contacts = [
        {
            Id: 1,
            Name: 'Amy Taylor',
            Title: 'VP of Engineering',
        },
        {
            Id: 2,
            Name: 'Michael Jones',
            Title: 'VP of Sales',
        },
        {
            Id: 3,
            Name: 'Jennifer Wu',
            Title: 'CEO',
        },
    ];

    /*
    render() {
        return this.templateOne ? templateOne : templateTwo;
    }
    */

    switchTemplate(){ 
        this.templateOne = this.templateOne === true ? false : true; 
    }

    handleChange(event) {
        this.areDetailsVisible = event.target.checked;
    }
}