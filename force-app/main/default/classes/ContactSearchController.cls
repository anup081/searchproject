//Class to contact search component
public with sharing class ContactSearchController {
     
    
    //fetch all contact records -adding comments in my org.
    //committing to gitlab 2
    @AuraEnabled  
   public static List<Contact> loadData(){  
     List<Contact> conList = [select Id,Name, FirstName, LastName, Email from Contact order by Name asc];  
     return conList;  
   }  
}