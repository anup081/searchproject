@isTest
public class ContactSearchControllerTest {
    
    //Testing fetching data from the contact object
    static testMethod void testgetcontacts() {
        Boolean success = true;
        try {
            Contact p = new Contact(FirstName = 'Hello', LastName = 'World', Email = 'hello@world.com');
            insert p;
	        ContactSearchController.loadData();
        } catch (Exception e) {
            success = false;
        } finally {
	        System.assert(success);
        }
    }
}
